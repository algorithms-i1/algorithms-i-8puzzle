/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {

    private int moves;
    private SearchNode finalNode;

    private class SearchNode implements Comparable<SearchNode> {
        private Board board;
        private int moves;
        private int manhattan;
        private SearchNode prev;

        public SearchNode(Board board, int moves, SearchNode prev) {
            this.board = board;
            this.moves = moves;
            this.prev = prev;
            this.manhattan = board.manhattan();
        }

        public int compareTo(SearchNode that) {
            return (manhattan + moves) - (that.manhattan + that.moves);
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) throw new IllegalArgumentException();
        MinPQ<SearchNode> frontierMain = new MinPQ<>();
        MinPQ<SearchNode> frontierTwin = new MinPQ<>();
        moves = 0;

        SearchNode nodeMain = new SearchNode(initial, 0, null);
        frontierMain.insert(nodeMain);
        SearchNode nodeTwin = new SearchNode(initial.twin(), 0, null);
        frontierTwin.insert(nodeTwin);

        do {
            nodeMain = frontierMain.delMin();
            nodeTwin = frontierTwin.delMin();
            if (nodeMain.board.isGoal() || nodeTwin.board.isGoal()) {
                moves = nodeMain.moves;
                finalNode = nodeMain;
                break;
            }
            for (Board neighbourMain : nodeMain.board.neighbors()
            ) {
                if (nodeMain.prev == null || !neighbourMain.equals(nodeMain.prev.board)) {
                    frontierMain.insert(new SearchNode(
                            neighbourMain, nodeMain.moves + 1, nodeMain));
                }
            }
            for (Board neighbourTwin : nodeTwin.board.neighbors()
            ) {
                if (nodeTwin.prev == null || !neighbourTwin.equals(nodeTwin.prev.board)) {
                    frontierTwin.insert(new SearchNode(
                            neighbourTwin, nodeTwin.moves + 1, nodeTwin));
                }
            }
        }
        while (!nodeMain.board.isGoal() && !nodeTwin.board.isGoal());
    }

    // is the initial board solvable? (see below)
    public boolean isSolvable() {
        return finalNode != null && finalNode.board.isGoal();
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        return isSolvable() ? moves : -1;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!isSolvable()) return null;
        Stack<Board> solution = new Stack<>();
        SearchNode node = finalNode;
        while (node != null) {
            solution.push(node.board);
            node = node.prev;
        }
        return solution;
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

}