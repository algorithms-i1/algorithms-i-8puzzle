import java.util.ArrayList;

public class Board {
    private int[][] tiles;
    private final int N;
    private int blankRow = -1;
    private int blankCol = -1;

    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles) {
        this.N = tiles[0].length;
        this.tiles = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                this.tiles[i][j] = tiles[i][j];
                if (tiles[i][j] == 0) {
                    blankRow = i;
                    blankCol = j;
                }
            }
        }
    }

    // string representation of this board
    public String toString() {
        StringBuilder rep = new StringBuilder(String.format("%d \n", N));
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                rep.append(" " + String.valueOf(tiles[i][j]));
            }
            rep.append("\n");
        }
        return rep.toString();
    }

    // board dimension n
    public int dimension() {
        return this.N;
    }

    // number of tiles out of place
    public int hamming() {
        int hamming = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                int index = mapTo1dIndex(i, j);
                int elem = tiles[i][j];
                if (elem != 0 && elem - 1 != index) {
                    hamming++;
                }
            }
        }
        return hamming;
    }

    private int mapTo1dIndex(int r, int c) {
        return (r) * N + c;
    }


    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int manhattan = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                int value = tiles[i][j];
                if (value != 0) {
                    int homeRow = (value - 1) / N;
                    int homeCol = (value - 1) % N;
                    manhattan += Math.abs(i - homeRow) + Math.abs(j - homeCol);
                }
            }
        }
        return manhattan;
    }

    // is this board the goal board?
    public boolean isGoal() {
        int[][] goalTiles = new int[N][N];
        int num = 1;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                goalTiles[i][j] = num++;
            }
        }
        goalTiles[N - 1][N - 1] = 0;
        Board goal = new Board(goalTiles);
        return equals(goal);
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        if (!this.toString().equals(y.toString())) return false;
        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        ArrayList<Board> neighbours = new ArrayList<>();
        int[][] neighbourCells = neighbourCells(blankRow, blankCol);
        for (int[] cell : neighbourCells
        ) {
            int nRow = cell[0], nCol = cell[1];
            if (isValidCell(nRow, nCol)) {
                int[][] nTiles = new int[N][N];
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        nTiles[i][j] = this.tiles[i][j];
                    }
                }
                nTiles[blankRow][blankCol] = this.tiles[nRow][nCol];
                nTiles[nRow][nCol] = 0;
                neighbours.add(new Board(nTiles));
            }
        }
        return neighbours;
    }


    private int[][] neighbourCells(int row, int col) {
        return new int[][] {
                { row + 1, col }, { row - 1, col }, { row, col - 1 },
                { row, col + 1 }
        };
    }


    private boolean isValidCell(int r, int c) {
        return (r >= 0 && r < N && c >= 0 && c < N);
    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {
        int[][] twinTiles = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                twinTiles[i][j] = this.tiles[i][j];
            }
        }
        int col1 = twinTiles[0][0] == 0 ? 1 : 0;
        int col2 = twinTiles[N / 2][N / 2] == 0 ? N / 2 - 1 : N / 2;
        int temp = twinTiles[0][col1];
        twinTiles[0][col1] = twinTiles[N / 2][col2];
        twinTiles[N / 2][col2] = temp;
        return new Board(twinTiles);
    }

    public static void main(String[] args) {
        int n = 3;
        int[][] tiles = new int[n][n];
        int num = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                tiles[i][j] = num++;
            }
        }
        Board b = new Board(tiles);
        System.out.println(b.toString());
        System.out.println(b.dimension());
        System.out.println(b.hamming());
        System.out.println(b.manhattan());
        System.out.println(b.isGoal());
        Board b2 = new Board(tiles);
        System.out.println(b.equals(b2));
        Iterable<Board> neighbours = b.neighbors();
        for (Board x : neighbours) System.out.println(x.toString());
        System.out.println(b.twin().toString());
    }


}