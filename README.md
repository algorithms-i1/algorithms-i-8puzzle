# Algorithms I Assignment 5: Kd-tree
Solution for assignment 4 of the Princeton University Algorithms I course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/8puzzle/specification.php).

The goal of this project was to efficiently solve puzzles of the "Slider Puzzle" style of any given size. 
Board.java is a representation of an nPuzzle board.
Solver.java utilizes a minimum priority queue and the A* search algorithm to always find the shortest possible path to a solution, or alternatively detect that the given board is unsolvable.

## Results  

Correctness:  52/52 tests passed

Memory:       22/22 tests passed

Timing:       125/125 tests passed

<b>Aggregate score:</b> 100.00%


## How to use
The class Solver.java accepts .txt representations of slider puzzles as command line arguments. The accepted format is: one line with an integer n, followed by n lines of n integers each, covering the integers from 0 through n**n - 1.
<b>NOTE:</b> Depends on the algs4 library used in the course.
